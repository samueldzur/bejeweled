﻿using System;
using BejeweledCore;
using BejeweledCore.Entity;
using BejeweledCore.Service;

namespace BejeweledConsole
{
    public class ConsoleUI
    {
        private string _playerName;
        private readonly Field _field;
        //private readonly IScoreService _scoreService = new ScoreServiceFile();
        //private readonly ICommentService _commentService = new CommentServiceFile();
        //private readonly IRatingService _ratingService = new RatingServiceFile();
        private readonly IScoreService _scoreService = new ScoreServiceEF();
        private readonly ICommentService _commentService = new CommentServiceEF();
        private readonly IRatingService _ratingService = new RatingServiceEF();
        public ConsoleUI(Field field)
        {
            //_scoreService.GetTopScores();
            //_ratingService.GetLastRatings();
            //_commentService.GetLastComments();
            _playerName = "";
            _field = field;
            _field.OnFieldChange += PrintField;
            _field.MoveRejected += () =>
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("MOVE REJECTED!");
            };
        }
        
        private void PrintField()
        {
            Console.Write("  ");
            for (var column = 1; column < _field.ColumnCount + 1; column++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(column);
                Console.Write(" ");
            }
            Console.WriteLine();
            for (var row = 0; row < _field.RowCount; row++)
            {   
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(row + 1);
                Console.Write(" ");
                for (var column = 0; column < _field.ColumnCount; column++)
                {
                    var tile = _field.GetTile(row, column);
                    char character = (char) tile.Type;
                    switch (character)
                    {
                        case 'A':
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;
                        case 'B':
                            Console.ForegroundColor = ConsoleColor.Blue;
                            break;
                        case 'C':
                            Console.ForegroundColor = ConsoleColor.Green;
                            break;
                        case 'D':
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            break;
                        case 'E':
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            break;
                        case 'F':
                            Console.ForegroundColor = ConsoleColor.Black;
                            break;
                        case 'G':
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            break;
                        case 'X':
                            Console.ForegroundColor = ConsoleColor.White;
                            break;
                    }
                    Console.Write(character + " ");
                }
                Console.WriteLine();
            }
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Player: " + _playerName + "   Score: " + _field.Score);
            Console.WriteLine();
        }

        private bool ProcessGameInput()
        {
            Console.ResetColor();
            Console.Write("Zadaj suradnice v tvare {row1 col1 row2 col2}: ");
            var line = Console.ReadLine();
            if (line != null)
            {
                if (line == "end")
                {
                    return true;
                }
                string[] coordinates = line.Split(" ");
                try
                {
                    _field.SwapTiles(Int32.Parse(coordinates[0]) - 1, Int32.Parse(coordinates[1]) - 1,
                        Int32.Parse(coordinates[2]) - 1, Int32.Parse(coordinates[3]) - 1);
                }
                catch (FormatException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine();
                    Console.WriteLine("Wrong input!");
                    Console.WriteLine();
                    Console.ResetColor();
                }
                catch (IndexOutOfRangeException)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine();
                    Console.WriteLine("Wrong coordinates!");
                    Console.WriteLine();
                    Console.ResetColor();
                }
            }

            return false;
        }

        /// <summary>
        /// prints "Bejeweled" to console
        /// </summary>
        private static void Welcome()
        {
            Console.WriteLine("**********************************************");
            Console.WriteLine("  ____       _                   _          _");
            Console.WriteLine(" |  _ \\     (_)                 | |        | |");
            Console.WriteLine(" | |_) | ___ _  _____      _____| | ___  __| |");
            Console.WriteLine(" |  _ < / _ \\ |/ _ \\ \\ /\\ / / _ \\ |/ _ \\/ _` |");
            Console.WriteLine(" | |_) |  __/ |  __/\\ V  V /  __/ |  __/ (_| |");
            Console.WriteLine(" |____/ \\___| |\\___| \\_/\\_/ \\___|_|\\___|\\__,_|");
            Console.WriteLine("           _/ |");
            Console.WriteLine("          |__/");
            Console.WriteLine("**********************************************");
        }

        private void ShowScores()
        {
            var list = _scoreService.GetTopScores();
            Console.WriteLine();
            Console.WriteLine("**********************************************");
            Console.WriteLine("                  TOP SCORES");
            Console.WriteLine("**********************************************");
            for (int i = 1; i < 11 && i < list.Count + 1; i++)
            {
                Console.WriteLine(i + ". " + list[i - 1].Player + " - " + list[i - 1].Points);
            }
            Console.WriteLine("**********************************************");
        }

        private void ShowComments()
        {
            var list = _commentService.GetLastComments();
            Console.WriteLine();
            Console.WriteLine("**********************************************");
            Console.WriteLine("                LAST COMMENTS");
            Console.WriteLine("**********************************************");
            for (int i = 1; i < 11 && i < list.Count + 1; i++)
            {
                Console.WriteLine(list[i - 1].Player + " - " + list[i - 1].CommentText);
            }
            Console.WriteLine("**********************************************");
        }

        private void ShowRatings()
        {
            var list = _ratingService.GetLastRatings();
            Console.WriteLine();
            Console.WriteLine("**********************************************");
            Console.WriteLine("                 LAST RATINGS");
            Console.WriteLine("**********************************************");
            for (int i = 1; i < 11 && i < list.Count + 1; i++)
            {
                Console.WriteLine(list[i - 1].Player + " " + list[i - 1].Stars);
            }
            Console.WriteLine("**********************************************");
        }
        
        /// <summary>
        /// home screen
        /// </summary>
        /// <returns>true if input == "exit"</returns>
        private bool ProcessHomeInput()
        {
            string input = "";
            while (input != "play")
            {
                Console.ResetColor();
                Console.WriteLine();
                Console.WriteLine("commands: play, show scores, show comments, show ratings, exit");
                Console.WriteLine();
                input = Console.ReadLine();
                switch (input)
                {
                    case "play":
                        return false;
                    case "show scores":
                        ShowScores();
                        break;
                    case "show comments":
                        ShowComments();
                        break;
                    case "show ratings":
                        ShowRatings();
                        break;
                    case "exit":
                        return true;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wrong input!");
                        break;
                }
            }
            return false;
        }

        private void ChangeName()
        {
            Console.ResetColor();
            Console.Write("Zadaj meno: ");
            _playerName = Console.ReadLine();
        }

        private void WriteComment()
        {
            Console.WriteLine();
            Console.ResetColor();
            Console.Write("comment text: ");
            string text = Console.ReadLine();
            _commentService.AddComment(new Comment {Player = _playerName, CommentText = text, TimeOfPublication = DateTime.Now});
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("THANK YOU FOR LEAVING A COMMENT!");
            Console.ResetColor();
        }

        private void RateGame()
        {
            Console.WriteLine();
            Console.ResetColor();
            Console.Write("Write 0-5 stars '*' to rate the game: ");
            string rate = Console.ReadLine();
            _ratingService.AddRating(new Rating {Player = _playerName, Stars = rate, TimeOfPublication = DateTime.Now});
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("THANK YOU FOR RATING THE GAME!");
            Console.ResetColor();
        }

        private bool ProcessAfterGameInput()
        {
            string input = "";
            while (input != "play again" && input != "exit" && input != "play")
            {
                Console.ResetColor();
                Console.WriteLine();
                Console.WriteLine("commands: play again, show scores, show comments, write a comment, show ratings, rate the game, change name, exit");
                Console.WriteLine();
                input = Console.ReadLine();
                switch (input)
                {
                    case "play again":
                        return true;
                    case "play":
                        return true;
                    case "show scores":
                        ShowScores();
                        break;
                    case "show comments":
                        ShowComments();
                        break;
                    case "write a comment":
                        WriteComment();
                        break;
                    case "show ratings":
                        ShowRatings();
                        break;
                    case "rate the game":
                        RateGame();
                        break;
                    case "change name":
                        ChangeName();
                        break;
                    case "exit":
                        return false;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wrong input!");
                        break;
                }
            }
            return false;
        }

        private void AfterGameReport(bool end)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            if (end)
            {
                Console.WriteLine();
                Console.WriteLine("YOU GAVE UP!");
            }
            else
            {
                Console.WriteLine("NO MORE AVAILABLE MOVES!");
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Congratulations, your score is " + _field.Score);
            Console.ResetColor();  
        }
        
        /// <summary>
        /// starts the game
        /// </summary>
        public void Play()
        {
            bool end = false;
            Welcome();
            if (ProcessHomeInput())
            {
                return;
            }
            ChangeName();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("NOTE:");
            Console.WriteLine("* You can end gameplay earlier by typing 'end' *");
            Console.WriteLine("* The score you earned will not be lost and will be listed *");
            Console.ResetColor();
            Console.WriteLine();
            PrintField();
            while (_field.MoveAvailable())
            {
                if (ProcessGameInput())
                {
                    end = true;
                    break;
                }
            }
            AfterGameReport(end);
            end = false;
            _scoreService.AddScore(new Score {Player = _playerName, Points = _field.Score, PlayedAt = DateTime.Now});
            while (ProcessAfterGameInput())
            {
                _field.Reset();
                Console.WriteLine();
                PrintField();
                while (_field.MoveAvailable())
                {
                    if (ProcessGameInput())
                    {
                        end = true;
                        break;
                    }
                }
                AfterGameReport(end);
                end = false;
                _scoreService.AddScore(new Score {Player = _playerName, Points = _field.Score, PlayedAt = DateTime.Now});
            }
        }
    }
}