﻿using System;
using System.Collections.Generic;
using System.Data;

namespace BejeweledCore
{
    [Serializable]
    public class Field
    {
        public event Action OnFieldChange;
        public event Action MoveRejected;
        private readonly Tile[,] _tiles;
        public int RowCount { get; }
        public int ColumnCount { get; }
        
        private DateTime _startTime;
        public int Score { get; private set; }
        
        /// <summary>
        /// constructor, creates a new field
        /// </summary>
        /// <param name="rowCount">number of rows in a new field</param>
        /// <param name="columnCount">number of columns in a new field</param>
        public Field(int rowCount, int columnCount)
        {
            if (rowCount < 3)
            {
                throw new ArgumentException("the minimum value of the parameter is equal to 3", nameof(rowCount));
            }

            if (columnCount < 3)
            {
                throw new ArgumentException("the minimum value of the parameter is equal to 3", nameof(columnCount));
            }
            RowCount = rowCount;
            ColumnCount = columnCount;
            _tiles = new Tile[rowCount, columnCount];
            Reset();
        }
        
        /// <summary>
        /// generates new tiles to the field, sets the score to 0
        /// </summary>
        public void Reset()
        {
            do
            {
                GenerateField();
            } while (!MoveAvailable());
            _startTime = DateTime.Now;
            Score = 0;
        }
        
        /// <summary>
        /// gets time that has elapsed since starting the game
        /// </summary>
        /// <returns>time from starting the game</returns>
        public TimeSpan GetTime()
        {
            return DateTime.Now - _startTime;
        }
        
        /// <summary>
        /// gets the tile in the field specified by its position
        /// </summary>
        /// <param name="row">row of tile in the field</param>
        /// <param name="column">column of tile in the field</param>
        /// <returns></returns>
        public Tile GetTile(int row, int column)
        {
            return _tiles[row, column];
        }
        
        /// <summary>
        /// generates a new field full of tiles
        /// </summary>
        private void GenerateField()
        {   
            Random random = new Random();
            for (var column = 0; column < ColumnCount; column++)
            {
                for (var row = 0; row < RowCount; row++)
                {
                    char randomChar;
                    do
                    {
                        randomChar = (char) random.Next('A', 'H');
                    } while (GenerateCondition(row, column, randomChar));
                    _tiles[row, column] = new Tile(randomChar);
                }
            }
        }
        
        /// <summary>
        /// checks if generated tile is same type as two tiles before in the row or the column
        /// </summary>
        /// <param name="row">row of generated tile</param>
        /// <param name="column">column of generated tile</param>
        /// <param name="character">type of generated tile</param>
        /// <returns>true if 3 same tiles in row or column</returns>
        private bool GenerateCondition(int row, int column, char character)
        {
            if (row >= 2)
            {
                if (_tiles[row - 1, column].Type == character && _tiles[row - 2, column].Type == character)
                {
                    return true;
                }
            }

            if (column >= 2)
            {
                if (_tiles[row, column - 1].Type == character && _tiles[row, column - 2].Type == character)
                {
                    return true;
                }
            }

            return false;
        }
        
        /// <summary>
        /// swaps any two tiles in the field
        /// </summary>
        /// <param name="firstTileRow">row of first tile</param>
        /// <param name="firstTileColumn">column of first tile</param>
        /// <param name="secondTileRow">row of second tile</param>
        /// <param name="secondTileColumn">column of second tile</param>
        public void SwapAnyTiles(int firstTileRow, int firstTileColumn, int secondTileRow, int secondTileColumn)
        {
            var temp = _tiles[firstTileRow, firstTileColumn];
            _tiles[firstTileRow, firstTileColumn] = _tiles[secondTileRow, secondTileColumn];
            _tiles[secondTileRow, secondTileColumn] = temp;
        }
        
        /// <summary>
        /// swaps two tiles if move is allowed and calls the method to pop tiles,
        /// creates magic tile if at least 5 tiles were poped by player with basic tiles
        /// </summary>
        /// <param name="firstTileRow">row of first tile</param>
        /// <param name="firstTileColumn">column of first tile</param>
        /// <param name="secondTileRow">row of second tile</param>
        /// <param name="secondTileColumn">column of second tile</param>
        /// <returns></returns>
        public bool SwapTiles(int firstTileRow, int firstTileColumn, int secondTileRow, int secondTileColumn)
        {
            if (firstTileRow == secondTileRow && firstTileColumn == secondTileColumn)
            {
                if (MoveRejected != null)
                {
                    MoveRejected();
                }
                return false;
            }
            if (firstTileRow != secondTileRow && firstTileColumn != secondTileColumn)
            {
                if (MoveRejected != null)
                {
                    MoveRejected();
                }
                return false;
            }
            if (Math.Abs(firstTileRow - secondTileRow) != 1 && Math.Abs(firstTileColumn - secondTileColumn) != 1)
            {
                if (MoveRejected != null)
                {
                    MoveRejected();
                }
                return false;
            }

            if (_tiles[firstTileRow, firstTileColumn].Type == _tiles[secondTileRow, secondTileColumn].Type &&
                _tiles[firstTileRow, firstTileColumn].Type == 'X')
            {
                PopAllTiles();
                do
                {
                    FallTiles();
                    FillEmptyTiles();
                }
                while (PopTiles().Item1 != -1);
                return true;
            }
            if (_tiles[firstTileRow, firstTileColumn].Type == 'X' ||
                _tiles[secondTileRow, secondTileColumn].Type == 'X')
            {
                Score++;
                if (_tiles[firstTileRow, firstTileColumn].Type == 'X')
                {
                    _tiles[firstTileRow, firstTileColumn] = new Tile(' ');
                    PopSameTiles((char)_tiles[secondTileRow, secondTileColumn].Type);
                }
                else
                {
                    _tiles[secondTileRow, secondTileColumn] = new Tile(' ');
                    PopSameTiles((char)_tiles[firstTileRow, firstTileColumn].Type);
                }
                do
                {
                    FallTiles();
                    FillEmptyTiles();
                }
                while (PopTiles().Item1 != -1);
                return true;
            }
            SwapAnyTiles(firstTileRow, firstTileColumn, secondTileRow, secondTileColumn);
            var returnValues = PopTiles(); // (score, row, column)
            if (returnValues.Item1 == -1) // if score == -1
            {
                SwapAnyTiles(firstTileRow, firstTileColumn, secondTileRow, secondTileColumn);
                if (MoveRejected != null)
                {
                    MoveRejected();
                }
                return false;
            }
            if (returnValues.Item1 >= 5) // if score >= 5
            {
                _tiles[returnValues.Item2, returnValues.Item3] = new Tile('X');
            }
            do
            {
                FallTiles();
                FillEmptyTiles();
            }
            while (PopTiles().Item1 != -1);
            return true;
        }
        
        /// <summary>
        /// pops same tiles in the field and
        /// </summary>
        /// <param name="type">type of tiles to be poped</param>
        public void PopSameTiles(char type, int magicRow = -1, int magicColumn = -1)
        {
            if (magicRow >= 0 && magicRow < RowCount && magicColumn >= 0 && magicColumn < ColumnCount)
            {
                Score++;
                _tiles[magicRow, magicColumn] = new Tile(' ');
            }
            for (var row = 0; row < RowCount; row++)
            {
                for (var column = 0; column < ColumnCount; column++)
                {
                    if (_tiles[row, column].Type == type)
                    {
                        Score++;
                        _tiles[row, column] = new Tile(' ');
                    }
                }
            }
            if (OnFieldChange != null)
            {
                OnFieldChange();
            }
        }
        
        /// <summary>
        /// the function pops all tiles in the field and
        /// </summary>
        public void PopAllTiles()
        {
            for (var row = 0; row < RowCount; row++)
            {
                for (var column = 0; column < ColumnCount; column++)
                {
                    _tiles[row, column] = new Tile(' ');
                    Score++;
                }
            }
            if (OnFieldChange != null)
            {
                OnFieldChange();
            }
        }
        
        /// <summary>
        /// pops tiles in the field if 3 same tiles in a row or a column and 
        /// </summary>
        /// <returns>tuple (number of tiles poped, position of new magic tile {row, column})</returns>
        public Tuple<int, int, int> PopTiles()
        {
            int magicRow = -1;
            int magicColumn = -1;
            Random random = new Random();
            bool magic = false;
            int randomInt = random.Next(5);
            List<int> rowPop = new List<int>();
            List<int> columnPop = new List<int>();
            for (var row = 0; row < RowCount; row++)
            {
                char character = (char) _tiles[row, 0].Type;
                int count = 0;
                for (var column = 0;  column < ColumnCount; column++)
                {
                    if (_tiles[row, column].Type == character)
                    {
                        count++;
                    }
                    else
                    {
                        if (count >= 3)
                        {
                            int number = 0;
                            for (var i = column - count; i < column; i++)
                            {
                                number++;
                                if (number == 3 && count >= 5 && character != ' ')
                                {
                                    magic = true;
                                    magicRow = row;
                                    magicColumn = i;
                                }
                                rowPop.Add(row);
                                columnPop.Add(i);
                            }   
                        }
                        character = (char) _tiles[row, column].Type;
                        count = 1;
                    }
                }
                if (count >= 3)
                {
                    int number = 0;
                    for (var i = ColumnCount - count; i < ColumnCount; i++)
                    {
                        number++;
                        if (number == 3 && count >= 5 && character != ' ')
                        {
                            magic = true;
                            magicRow = row;
                            magicColumn = i;
                        }
                        rowPop.Add(row);
                        columnPop.Add(i);
                    }   
                }
            }

            for (var column = 0; column < ColumnCount; column++)
            {
                char character = (char) _tiles[0, column].Type;
                int count = 0;
                for (var row = 0; row < RowCount; row++)
                {
                    if (_tiles[row, column].Type == character)
                    {
                        count++;
                    }
                    else
                    {
                        if (count >= 3)
                        {
                            int number = 0;
                            for (var i = row - count; i < row; i++)
                            {
                                number++;
                                if (number == 3 && count >= 5 && character != ' ')
                                {
                                    magic = true;
                                    magicRow = i;
                                    magicColumn = column;
                                }
                                rowPop.Add(i);
                                columnPop.Add(column);
                            }
                        }
                        character = (char) _tiles[row, column].Type;
                        count = 1;
                    }
                }
                if (count >= 3)
                {
                    int number = 0;
                    for (var i = RowCount - count; i < RowCount; i++)
                    {
                        number++;
                        if (number == 3 && count >= 5 && character != ' ')
                        {
                            magic = true;
                            magicRow = i;
                            magicColumn = column;
                        }
                        rowPop.Add(i);
                        columnPop.Add(column);
                    }   
                }
            }

            if (rowPop.Count == 0)
            {
                return Tuple.Create(-1, -1, -1);
            }

            for (var i = 0; i < rowPop.Count; i++)
            {
                if (_tiles[rowPop[i], columnPop[i]].Type != ' ')
                {
                    Score++;
                    _tiles[rowPop[i], columnPop[i]] = new Tile(' ');
                }
            }

            if (OnFieldChange != null)
            {
                OnFieldChange();
            }

            if (magic)
            {
                _tiles[magicRow, magicColumn] = new Tile('X');
            }
            return Tuple.Create(rowPop.Count, magicRow, magicColumn);
        }

        /// <summary>
        /// if there are empty spaces in the field, tiles above these spaces fall down,
        /// empty spaces are moved on top of the field
        /// </summary>
        public void FallTiles()
        {
            for (var column = 0; column < ColumnCount; column++)
            {
                int index = RowCount - 2;
                for (var row = RowCount - 1; row > 0; row--)
                {
                    if (index < 0)
                    {
                        continue;
                    }
                    if (_tiles[row, column].Type == ' ')
                    {
                        while (_tiles[index, column].Type == ' ')
                        {
                            index--;
                            if (index <= 0)
                            {
                                break;
                            }
                        }
                        if (index < 0)
                        {
                            break;
                        }
                        SwapAnyTiles(row, column, index, column);
                    }
                    index--;
                }
            }
            if (OnFieldChange != null)
            {
                OnFieldChange();
            }
        }
        
        /// <summary>
        /// fills up empty spaces in the field with new random tiles
        /// </summary>
        public void FillEmptyTiles()
        {
            Random random = new Random();
            for (var column = 0; column < ColumnCount; column++)
            {
                for (var row = 0; row < RowCount; row++)
                {
                    if (_tiles[row, column].Type == ' ')
                    {
                        _tiles[row, column] = new Tile((char) random.Next('A', 'H'));
                    }
                }
            }
            if (OnFieldChange != null)
            {
                OnFieldChange();
            }
        }

        /// <summary>
        /// checks if a move in the field is possible
        /// </summary>
        /// <returns>true if a move is possible</returns>
        public bool MoveAvailable()
        {
            for (var row = 0; row < RowCount; row++)
            {
                for (var column = 0; column < ColumnCount - 2; column++)
                {
                    if (_tiles[row, column].Type == _tiles[row, column + 2].Type)
                    {
                        try
                        {
                            if (_tiles[row, column].Type == _tiles[row + 1, column + 1].Type)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row, column].Type == _tiles[row - 1, column + 1].Type)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                    }
                }
                if (_tiles[row, 0].Type == 'X')
                {
                    return true;
                }
                char character = (char) _tiles[row, 0].Type;
                for (var column = 1; column < ColumnCount; column++)
                {
                    if (_tiles[row, column].Type == 'X')
                    {
                        return true;
                    }
                    if (character == _tiles[row, column].Type)
                    {
                        try
                        {
                            if (_tiles[row, column + 2].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row, column - 3].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row - 1, column + 1].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row - 1, column - 2].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row + 1, column + 1].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row + 1, column - 2].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                    }
                    else
                    {
                        character = (char) _tiles[row, column].Type;
                    }
                }
            }

            for (var column = 0; column < ColumnCount; column++)
            {   
                for (var row = 0; row < RowCount - 2; row++)
                {
                    if (_tiles[row, column].Type == _tiles[row + 2, column].Type)
                    {
                        try
                        {
                            if (_tiles[row, column].Type == _tiles[row + 1, column + 1].Type)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row, column].Type == _tiles[row + 1, column - 1].Type)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                    }
                }
                char character = (char) _tiles[0, column].Type;
                for (var row = 1; row < RowCount; row++)
                {
                    if (character == _tiles[row, column].Type)
                    {
                        try
                        {
                            if (_tiles[row + 2, column].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row - 3, column].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row - 2, column - 1].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row + 1, column - 1].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row - 2, column + 1].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                        try
                        {
                            if (_tiles[row + 1, column + 1].Type == character)
                            {
                                return true;
                            }
                        }
                        catch (IndexOutOfRangeException) {}
                    }
                    else
                    {
                        character = (char) _tiles[row, column].Type;
                    }
                }
            }
            return false;
        }
    }
}