﻿using System;

namespace BejeweledCore
{
    [Serializable]
    public class Tile
    {
        public int Type { get; }

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="type">type of a new tile</param>
        public Tile(char type)
        {
            Type = type;
        }
    }
}