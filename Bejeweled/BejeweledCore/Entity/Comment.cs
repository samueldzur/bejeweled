﻿using System;

namespace BejeweledCore.Entity
{
    [Serializable]
    public class Comment
    {
        public int Id { get; set; }
        public string Player { get; set; }
        public string CommentText { get; set; }
        public DateTime TimeOfPublication { get; set; }
    }
}