﻿using System;

namespace BejeweledCore.Entity
{
    [Serializable]
    public class Rating
    {
        public int Id { get; set; }
        public string Player { get; set; }
        public string Stars { get; set; }
        public DateTime TimeOfPublication { get; set; }
    }
}