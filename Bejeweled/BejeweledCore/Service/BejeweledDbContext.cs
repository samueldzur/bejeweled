﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BejeweledCore.Entity;
using Microsoft.EntityFrameworkCore;

namespace BejeweledCore.Service
{
    public class BejeweledDbContext : DbContext
    {
        public DbSet<Score> Scores { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Rating> Ratings { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=Bejeweled;Trusted_Connection=True;");
        }
    }
}
