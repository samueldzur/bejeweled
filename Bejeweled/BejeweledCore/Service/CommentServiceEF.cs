﻿using BejeweledCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BejeweledCore.Service
{
    public class CommentServiceEF : ICommentService
    {
        public void AddComment(Comment comment)
        {
            using (var context = new BejeweledDbContext())
            {
                context.Comments.Add(comment);
                context.SaveChanges();
            }
        }

        public IList<Comment> GetLastComments()
        {
            using (var context = new BejeweledDbContext())
            {
                return (from c in context.Comments orderby c.TimeOfPublication descending select c).Take(10).ToList();
            }
        }

        public void ResetComments()
        {
            using (var context = new BejeweledDbContext())
            {
                context.Database.ExecuteSqlRaw("DELETE FROM Comments");
            }
        }
    }
}
