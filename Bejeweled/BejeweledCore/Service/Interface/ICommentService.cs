﻿using System.Collections.Generic;
using BejeweledCore.Entity;

namespace BejeweledCore.Service
{
    public interface ICommentService
    {
        void AddComment(Comment comment);
        IList<Comment> GetLastComments();
        void ResetComments();
    }
}