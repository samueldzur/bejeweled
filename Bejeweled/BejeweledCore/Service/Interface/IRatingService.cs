﻿using System.Collections.Generic;
using BejeweledCore.Entity;

namespace BejeweledCore.Service
{
    public interface IRatingService
    {
        void AddRating(Rating rating);
        IList<Rating> GetLastRatings();
        void ResetRatings();
    }
}