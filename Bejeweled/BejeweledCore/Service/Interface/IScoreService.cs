﻿using System.Collections.Generic;
using BejeweledCore.Entity;

namespace BejeweledCore.Service
{
    public interface IScoreService
    {
        void AddScore(Score score);
        IList<Score> GetTopScores();
        void ResetScores();
    }
}