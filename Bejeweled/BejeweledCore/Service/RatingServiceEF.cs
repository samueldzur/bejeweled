﻿using BejeweledCore.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BejeweledCore.Service
{
    public class RatingServiceEF : IRatingService
    {
        public void AddRating(Rating rating)
        {
            int count = rating.Stars.Count(c => c == '*');
            if (rating.Stars.Length > 5 || rating.Stars.Length != count)
            {
                return;
            }

            using (var context = new BejeweledDbContext())
            {
                context.Ratings.Add(rating);
                context.SaveChanges();
            }
        }

        public IList<Rating> GetLastRatings()
        {
            using (var context = new BejeweledDbContext())
            {
                return (from r in context.Ratings orderby r.TimeOfPublication descending select r).Take(10).ToList();
            }
        }

        public double GetAverageRating()
        {
            using (var context = new BejeweledDbContext())
            {
                return context.Ratings.Average(x => x.Stars.Length);
            }
        }

        public void ResetRatings()
        {
            using (var context = new BejeweledDbContext())
            {
                context.Database.ExecuteSqlRaw("DELETE FROM Ratings");
            }
        }
    }
}
