﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using BejeweledCore.Entity;

namespace BejeweledCore.Service
{
    public class RatingServiceFile : IRatingService
    {
        private const string FileName = "rating.bin";
        private IList<Rating> _ratings = new List<Rating>();
        
        public void AddRating(Rating rating)
        {
            int count = rating.Stars.Count(c => c == '*');
            if (rating.Stars.Length > 5 || rating.Stars.Length != count)
            {
                return;
            }
            _ratings.Add(rating);
            SaveRatings();
        }

        public IList<Rating> GetLastRatings()
        {
            LoadRatings();
            return _ratings.OrderByDescending(r => r.TimeOfPublication).ToList();
        }

        public void ResetRatings()
        {
            _ratings.Clear();
            File.Delete(FileName);
        }
        
        private void SaveRatings()
        {
            using (var fs = File.OpenWrite(FileName))
            {
                var bf = new BinaryFormatter();
                bf.Serialize(fs, _ratings);
            }
        }

        private void LoadRatings()
        {
            if (File.Exists(FileName))
            {
                using (var fs = File.OpenRead(FileName))
                {
                    var bf = new BinaryFormatter();
                    _ratings = (List<Rating>) bf.Deserialize(fs);
                }
            }
        }
    }
}