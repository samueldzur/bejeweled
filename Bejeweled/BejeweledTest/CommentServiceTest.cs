﻿using System;
using BejeweledCore.Entity;
using BejeweledCore.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BejeweledTest
{
    [TestClass]
    public class CommentServiceTest
    {
        [TestMethod]
        public void AddTest1()
        {
            var service = CreateService();
            service.ResetComments();
            service.AddComment(new Comment {Player = "Samuel", CommentText = "Great game!!!", TimeOfPublication = DateTime.Now});
            
            Assert.AreEqual(1, service.GetLastComments().Count);
            Assert.AreEqual("Samuel", service.GetLastComments()[0].Player);
            Assert.AreEqual("Great game!!!", service.GetLastComments()[0].CommentText);
            service.ResetComments();
        }

        [TestMethod]
        public void AddTest2()
        {
            var service = CreateService();
            service.ResetComments();
            service.AddComment(new Comment {Player = "Samuel", CommentText = "Great game!!!", TimeOfPublication = DateTime.Now});
            service.AddComment(new Comment {Player = "Jozo", CommentText = "It was fun", TimeOfPublication = DateTime.Now});
            service.AddComment(new Comment {Player = "Ivan", CommentText = "I played it all night long!", TimeOfPublication = DateTime.Now});
            
            Assert.AreEqual(3, service.GetLastComments().Count);
            
            Assert.AreEqual("Ivan", service.GetLastComments()[0].Player);
            Assert.AreEqual("I played it all night long!", service.GetLastComments()[0].CommentText);
            
            Assert.AreEqual("Jozo", service.GetLastComments()[1].Player);
            Assert.AreEqual("It was fun",service.GetLastComments()[1].CommentText);
            
            Assert.AreEqual("Samuel", service.GetLastComments()[2].Player);
            Assert.AreEqual("Great game!!!", service.GetLastComments()[2].CommentText);
            service.ResetComments();
        }

        [TestMethod]
        public void ResetTest()
        {
            var service = CreateService();
            service.ResetComments();
            service.AddComment(new Comment {Player = "Samuel", CommentText = "Great game!!!", TimeOfPublication = DateTime.Now});
            service.AddComment(new Comment {Player = "Jozo", CommentText = "It was fun", TimeOfPublication = DateTime.Now});
            service.ResetComments();
            
            Assert.AreEqual(0, service.GetLastComments().Count);
        }

        private ICommentService CreateService()
        {
            //return new CommentServiceFile();
            return new CommentServiceEF();
        } 
    }
}