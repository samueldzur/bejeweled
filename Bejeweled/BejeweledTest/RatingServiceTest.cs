﻿using System;
using BejeweledCore.Entity;
using BejeweledCore.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BejeweledTest
{
    [TestClass]
    public class RatingServiceTest
    {
        [TestMethod]
        public void AddTest1()
        {
            var service = CreateService();
            service.ResetRatings();
            service.AddRating(new Rating {Player = "Samuel", Stars = "**", TimeOfPublication = DateTime.Now});
            
            Assert.AreEqual(1, service.GetLastRatings().Count);
            Assert.AreEqual("Samuel", service.GetLastRatings()[0].Player);
            Assert.AreEqual("**", service.GetLastRatings()[0].Stars);
            service.ResetRatings();
        }

        [TestMethod]
        public void AddTest2()
        {
            var service = CreateService();
            service.ResetRatings();
            service.AddRating(new Rating {Player = "Samuel", Stars = "", TimeOfPublication = DateTime.Now});
            
            Assert.AreEqual(1, service.GetLastRatings().Count);
            Assert.AreEqual("Samuel", service.GetLastRatings()[0].Player);
            Assert.AreEqual("", service.GetLastRatings()[0].Stars);
            service.ResetRatings();
        }

        [TestMethod]
        public void AddTest3()
        {
            var service = CreateService();
            service.ResetRatings();
            service.AddRating(new Rating {Player = "Samuel", Stars = "*****", TimeOfPublication = DateTime.Now});
            
            Assert.AreEqual(1, service.GetLastRatings().Count);
            Assert.AreEqual("Samuel", service.GetLastRatings()[0].Player);
            Assert.AreEqual("*****", service.GetLastRatings()[0].Stars);
            service.ResetRatings();
        }

        [TestMethod]
        public void AddTest4()
        {
            var service = CreateService();
            service.ResetRatings();
            service.AddRating(new Rating {Player = "Samuel", Stars = "******", TimeOfPublication = DateTime.Now});
            Assert.AreEqual(0, service.GetLastRatings().Count);
            service.ResetRatings();
        }

        [TestMethod]
        public void AddTest5()
        {
            var service = CreateService();
            service.ResetRatings();
            service.AddRating(new Rating {Player = "Samuel", Stars = "abc", TimeOfPublication = DateTime.Now});
            Assert.AreEqual(0, service.GetLastRatings().Count);
            service.ResetRatings();
        }
        
        [TestMethod]
        public void AddTest6()
        {
            var service = CreateService();
            service.ResetRatings();
            service.AddRating(new Rating {Player = "Samuel", Stars = "***", TimeOfPublication = DateTime.Now});
            service.AddRating(new Rating {Player = "Lubo", Stars = "**d", TimeOfPublication = DateTime.Now});
            service.AddRating(new Rating {Player = "Peter", Stars = "****", TimeOfPublication = DateTime.Now});
            service.AddRating(new Rating {Player = "Stevo", Stars = "*", TimeOfPublication = DateTime.Now});
            
            Assert.AreEqual(3, service.GetLastRatings().Count);
            
            Assert.AreEqual("Stevo", service.GetLastRatings()[0].Player);
            Assert.AreEqual("*", service.GetLastRatings()[0].Stars);
            
            Assert.AreEqual("Peter", service.GetLastRatings()[1].Player);
            Assert.AreEqual("****", service.GetLastRatings()[1].Stars);
            
            Assert.AreEqual("Samuel", service.GetLastRatings()[2].Player);
            Assert.AreEqual("***", service.GetLastRatings()[2].Stars);
            
            service.ResetRatings();
        }

        [TestMethod]
        public void ResetTest()
        {
            var service = CreateService();
            service.ResetRatings();
            service.AddRating(new Rating {Player = "Peter", Stars = "****", TimeOfPublication = DateTime.Now});
            service.AddRating(new Rating {Player = "Stevo", Stars = "*", TimeOfPublication = DateTime.Now});
            service.ResetRatings();
            
            Assert.AreEqual(0, service.GetLastRatings().Count);
        }
        
        private IRatingService CreateService()
        {
            //return new RatingServiceFile();
            return new RatingServiceEF();
        }
    }
}