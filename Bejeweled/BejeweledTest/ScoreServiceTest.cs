using System;
using BejeweledCore.Entity;
using BejeweledCore.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BejeweledTest
{
    [TestClass]
    public class ScoreServiceTest
    {
        [TestMethod]
        public void AddTest1()
        {
            var service = CreateService();
            service.ResetScores();
            service.AddScore(new Score {Player = "Samuel", Points = 430, PlayedAt = DateTime.Now});
            
            Assert.AreEqual(1, service.GetTopScores().Count);
            Assert.AreEqual("Samuel", service.GetTopScores()[0].Player);
            Assert.AreEqual(430, service.GetTopScores()[0].Points);
        }
        
        [TestMethod]
        public void AddTest2()
        {
            var service = CreateService();
            service.ResetScores();
            service.AddScore(new Score {Player = "Samuel", Points = 430, PlayedAt = DateTime.Now});
            service.AddScore(new Score {Player = "Peter", Points = 500, PlayedAt = DateTime.Now});
            service.AddScore(new Score {Player = "Michal", Points = 400, PlayedAt = DateTime.Now});
            
            Assert.AreEqual(3, service.GetTopScores().Count);
            
            Assert.AreEqual("Peter", service.GetTopScores()[0].Player);
            Assert.AreEqual(500, service.GetTopScores()[0].Points);
            
            Assert.AreEqual("Samuel", service.GetTopScores()[1].Player);
            Assert.AreEqual(430, service.GetTopScores()[1].Points);
            
            Assert.AreEqual("Michal", service.GetTopScores()[2].Player);
            Assert.AreEqual(400, service.GetTopScores()[2].Points);
            service.ResetScores();
        }

        [TestMethod]
        public void ResetTest()
        {
            var service = CreateService();
            service.ResetScores();
            service.AddScore(new Score {Player = "Samuel", Points = 430, PlayedAt = DateTime.Now});
            service.AddScore(new Score {Player = "Michal", Points = 400, PlayedAt = DateTime.Now});
            service.ResetScores();
            
            Assert.AreEqual(0, service.GetTopScores().Count);
        }

        private IScoreService CreateService()
        {
            //return new ScoreServiceFile();
            return new ScoreServiceEF();
        }
    }
}