﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BejeweledCore.Entity;
using BejeweledCore.Service;

namespace BejeweledWeb.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private ICommentService _commentService = new CommentServiceEF();

        [HttpGet]
        public IEnumerable<Comment> GetComments()
        {
            return _commentService.GetLastComments();
        }

        [HttpPost]
        public void PostComment(Comment comment)
        {
            if (comment.TimeOfPublication == DateTime.MinValue)
            {
                comment.TimeOfPublication = DateTime.Now;
            }
            _commentService.AddComment(comment);
        }
    }
}
