﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BejeweledCore.Entity;
using BejeweledCore.Service;

namespace BejeweledWeb.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private IRatingService _ratingService = new RatingServiceEF();

        [HttpGet]
        public IEnumerable<Rating> GetRatings()
        {
            return _ratingService.GetLastRatings();
        }

        [HttpPost]
        public void PostRating(Rating rating)
        {
            if (rating.TimeOfPublication == DateTime.MinValue)
            {
                rating.TimeOfPublication = DateTime.Now;
            }
            _ratingService.AddRating(rating);
        }
    }
}
