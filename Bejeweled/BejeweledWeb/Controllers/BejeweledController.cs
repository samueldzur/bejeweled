﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BejeweledCore;
using BejeweledCore.Entity;
using BejeweledCore.Service;
using Microsoft.AspNetCore.Http;

namespace BejeweledWeb.Controllers
{
    public class BejeweledController : Controller
    {
        const string FieldSessionKey = "field";
        const string RowSessionKey = "row";
        const string ColumnSessionKey = "column";
        const string SecondRowSessionKey = "secondRow";
        const string SecondColumnSessionKey = "secondColumn";
        const string FirstSelectedSessionKey = "firstSelected";
        const string MoveSessionKey = "move";
        const string PlayerSessionKey = "player";

        public IActionResult Index()
        {
            return View("Index");
        }

        public IActionResult SetNameView()
        {
            return View("SetName");
        }

        public IActionResult SetName(string name)
        {
            HttpContext.Session.SetObject(PlayerSessionKey, name);
            return Index();
        }

        public IActionResult NewGame()
        {
            var move = true;
            HttpContext.Session.SetObject(MoveSessionKey, move);
            var firstSelected = false;
            HttpContext.Session.SetObject(FirstSelectedSessionKey, firstSelected);
            var field = new Field(8, 8);
            HttpContext.Session.SetObject(FieldSessionKey, field);
            return View("Field", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
        }

        public IActionResult TopScores()
        {
            var _scoreService = new ScoreServiceEF();
            var scores = _scoreService.GetTopScores();
            return View("TopScores", scores);
        }

        public IActionResult Ratings()
        {
            var _ratingService = new RatingServiceEF();
            var ratings = _ratingService.GetLastRatings();
            return View("Ratings", (ratings, _ratingService.GetAverageRating()));
        }

        public IActionResult Comments()
        {
            var _commentService = new CommentServiceEF();
            var comments = _commentService.GetLastComments();
            return View("Comments", comments);
        }

        public IActionResult Select(int row, int column)
        {
            var move = (bool) HttpContext.Session.GetObject(MoveSessionKey);
            var field = (Field)HttpContext.Session.GetObject(FieldSessionKey);
            if (!move)
            {
                return View("PopTiles", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
            }
            var firstSelected = (bool) HttpContext.Session.GetObject(FirstSelectedSessionKey);
            if (firstSelected)
            {
                var firstRow = (int)HttpContext.Session.GetObject(RowSessionKey);
                var firstColumn = (int)HttpContext.Session.GetObject(ColumnSessionKey);
                if ((firstRow == row && Math.Abs(firstColumn - column) == 1) || (firstColumn == column && Math.Abs(firstRow - row) == 1))
                {
                    field.SwapAnyTiles(firstRow, firstColumn, row, column);
                    HttpContext.Session.SetObject(FieldSessionKey, field);
                    HttpContext.Session.SetObject(SecondRowSessionKey, row);
                    HttpContext.Session.SetObject(SecondColumnSessionKey, column);
                    HttpContext.Session.SetObject(FirstSelectedSessionKey, false);
                    HttpContext.Session.SetObject(MoveSessionKey, false);
                    return View("PopTiles", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
                }
                else
                {
                    HttpContext.Session.SetObject(RowSessionKey, row);
                    HttpContext.Session.SetObject(ColumnSessionKey, column);
                }
                HttpContext.Session.SetObject(FieldSessionKey, field);
            }
            else
            {
                HttpContext.Session.SetObject(RowSessionKey, row);
                HttpContext.Session.SetObject(ColumnSessionKey, column);
                HttpContext.Session.SetObject(FirstSelectedSessionKey, true);
            }
            return View("Field", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
        }

        public IActionResult FallTiles()
        {
            var field = (Field)HttpContext.Session.GetObject(FieldSessionKey);
            field.FallTiles();
            HttpContext.Session.SetObject(FieldSessionKey, field);
            HttpContext.Session.SetObject(MoveSessionKey, true);
            return View("ShowFalled", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
        }

        public IActionResult FillEmpty()
        {
            var field = (Field)HttpContext.Session.GetObject(FieldSessionKey);
            field.FillEmptyTiles();
            HttpContext.Session.SetObject(FieldSessionKey, field);
            return View("ShowFilled", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
        }

        public IActionResult EndGame()
        {
            var field = (Field)HttpContext.Session.GetObject(FieldSessionKey);
            var player = (string)HttpContext.Session.GetObject(PlayerSessionKey);
            var _scoreService = new ScoreServiceEF();
            _scoreService.AddScore(new Score() {PlayedAt = DateTime.Now, Player = player, Points = field.Score});
            return View("EndGame", field.Score);
        }

        public IActionResult PopDecide()
        {
            var field = (Field) HttpContext.Session.GetObject(FieldSessionKey);
            if (field.PopTiles().Item1 == -1)
            {
                if (!field.MoveAvailable())
                {
                    return EndGame();
                }
                return View("Field", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
            }
            HttpContext.Session.SetObject(FieldSessionKey, field);
            return View("ShowPopped", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
        }

        public IActionResult ShowPopped()
        {
            var field = (Field)HttpContext.Session.GetObject(FieldSessionKey);
            if ((bool)HttpContext.Session.GetObject(MoveSessionKey))
            {
                return View("Field", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
            }
            var firstRow = (int)HttpContext.Session.GetObject(RowSessionKey);
            var firstColumn = (int)HttpContext.Session.GetObject(ColumnSessionKey);
            var secondRow = (int) HttpContext.Session.GetObject(SecondRowSessionKey);
            var secondColumn = (int) HttpContext.Session.GetObject(SecondColumnSessionKey);
            if (field.GetTile(firstRow, firstColumn).Type == 'X' && field.GetTile(secondRow, secondColumn).Type == 'X')
            {
                field.PopAllTiles();
            }
            else if (field.GetTile(firstRow, firstColumn).Type == 'X')
            {
                field.PopSameTiles((char)field.GetTile(secondRow, secondColumn).Type, firstRow, firstColumn);
            }
            else if (field.GetTile(secondRow, secondColumn).Type == 'X')
            {
                field.PopSameTiles((char)field.GetTile(firstRow, firstColumn).Type, secondRow, secondColumn);
            }
            else if (field.PopTiles().Item1 == -1)
            {
                field.SwapAnyTiles(firstRow, firstColumn, secondRow, secondColumn);
                HttpContext.Session.SetObject(FieldSessionKey, field);
                HttpContext.Session.SetObject(MoveSessionKey, true);
                return View("Field", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
            }
            HttpContext.Session.SetObject(FieldSessionKey, field);
            return View("ShowPopped", (field, HttpContext.Session.GetObject(PlayerSessionKey)));
        }

        public IActionResult AddComment()
        {
            return View("AddComment");
        }

        public IActionResult SaveComment(string comment)
        {
            var player = (string) HttpContext.Session.GetObject(PlayerSessionKey);
            var _commentService = new CommentServiceEF();
            _commentService.AddComment(new Comment() {TimeOfPublication = DateTime.Now, Player = player, CommentText = comment});
            return View("AddCommentSuccess");
        }

        public IActionResult AddRating()
        {
            return View("AddRating");
        }

        public IActionResult SaveRating(int stars)
        {
            var player = (string)HttpContext.Session.GetObject(PlayerSessionKey);
            var _ratingService = new RatingServiceEF();
            _ratingService.AddRating(new Rating() {TimeOfPublication = DateTime.Now, Player = player, Stars = new string('*', stars)});
            return View("AddRatingSuccess");
        }
    }
}
