﻿using System;
using BejeweledCore.Entity;
using BejeweledCore.Service;

namespace InsertServiceData
{
    class Program
    {
        static void Main(string[] args)
        {
            IScoreService scoreService = new ScoreServiceFile();
            ICommentService commentService = new CommentServiceFile();
            IRatingService ratingService = new RatingServiceFile();
            
            commentService.AddComment(new Comment {Player = "Samuel", CommentText = "Great game!!!", TimeOfPublication = DateTime.Now});
            commentService.AddComment(new Comment {Player = "Jozo", CommentText = "It was fun", TimeOfPublication = DateTime.Now});
            commentService.AddComment(new Comment {Player = "Ivan", CommentText = "I played it all night long!", TimeOfPublication = DateTime.Now});
            
            scoreService.AddScore(new Score {Player = "Samuel", Points = 430, PlayedAt = DateTime.Now});
            scoreService.AddScore(new Score {Player = "Peter", Points = 500, PlayedAt = DateTime.Now});
            scoreService.AddScore(new Score {Player = "Michal", Points = 400, PlayedAt = DateTime.Now});
            
            ratingService.AddRating(new Rating {Player = "Samuel", Stars = "***", TimeOfPublication = DateTime.Now});
            ratingService.AddRating(new Rating {Player = "Lubo", Stars = "**d", TimeOfPublication = DateTime.Now});
            ratingService.AddRating(new Rating {Player = "Peter", Stars = "****", TimeOfPublication = DateTime.Now});
        }
    }
}